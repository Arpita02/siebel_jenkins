prompt -> Starting with Repository Rename Activity....
prompt - - - - - - - - - -

prompt -> Below is the existing Active repository
prompt - - - - - - - - - -

select row_id, created, name, comments, inactive_flg from siebel.s_repository where name = 'Siebel Repository_16-FEB-19' and (inactive_flg = 'Y' or inactive_flg is null);


prompt -> Taking backup of the existing Active repository
prompt - - - - - - - - - -


update siebel.s_repository set name = name||'_'||sysdate where name = 'Siebel Repository_16-FEB-19' and (inactive_flg = 'Y' or inactive_flg is null);
commit;

prompt -> Backup taken of the existing Active repository
prompt - - - - - - - - - -

prompt Press any key to continue....


prompt -> Below is the new imported repository
prompt - - - - - - - - - -

select row_id, Created, inactive_flg, name  from siebel.s_repository where created = (select max(created) from siebel.s_repository) and (inactive_flg = 'N' or inactive_flg is null);

prompt Press any key to continue....


prompt -> Renaming the new imported repository and making it Active Siebel repository
prompt - - - - - - - - - -

prompt Press any key to continue....


update siebel.s_repository set name = 'Siebel Repository Testing_C61' , inactive_flg = null, comments = 'Deployed on '||sysdate where created = (select max(created) from siebel.s_repository) and (inactive_flg = 'N' or inactive_flg is null);
commit;

prompt -> New imported repository is renamed and now an Active Siebel repository!
prompt - - - - - - - - - -
exit;
